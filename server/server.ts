import express from 'express'
import cors from 'cors'
import * as listeningOn from 'listening-on'
import { join, resolve } from 'path'
import { config } from 'dotenv'
import { UserController } from './user.controller'
import { UserService } from './user.service'
import { knex } from './knex'
import { JWT_SECRET, PORT } from './env'
import { GradeService } from './grade.service'
import { GradeController } from './grade.controller'
import { GuardController } from './guard.controller'
import { SMSService } from './SMS.service'
config()

const app = express()

app.use(cors())

app.use(express.static('public'))

app.use(express.json() as any)
app.use(express.urlencoded({ extended: true }) as any)

app.use((req, res, next) => {
  if (req.method === 'GET') {
    console.log(req.method, req.url)
  } else {
    console.log(req.method, req.url, req.body)
  }
  next()
})

let guard = new GuardController(JWT_SECRET)

let sms = new SMSService()

let userService = new UserService(knex, sms, JWT_SECRET)
let userController = new UserController(userService, guard)
app.use(userController.router)

let gradeService = new GradeService(knex)
let gradeController = new GradeController(gradeService)
app.use(gradeController.router)

app.use((req, res) => {
  // res.status(404).sendFile(resolve(join('public', '404.html')))
  res
    .status(404)
    .json({ error: `Route not found. Method: ${req.method}, url: ${req.url}` })
})

app.listen(PORT, () => {
  listeningOn.print(PORT)
})
