import { Knex } from 'knex'
import jwtSimple from 'jwt-simple'
import { JWTPayload, NewProfile } from './types'
import { SMSService } from './SMS.service'

export class UserService {
  constructor(
    public knex: Knex,
    public sms: SMSService,
    public JWT_SECRET: string,
  ) {}

  // code -> user.id
  code_verifyTel_map = new Map<
    string,
    { id: number; tel: string; timer: any }
  >()

  private genCode() {
    let trial = 0
    for (;;) {
      trial++
      let code = Math.random().toString().slice(-6)
      if (!this.code_verifyTel_map.has(code)) {
        return code
      }
      if (trial > 10) {
        throw new Error('verification code pool is full')
      }
    }
  }

  async changeName(id: number, name: string) {
    await this.knex('user').update({ name }).where({ id })
    return {}
  }

  async requestTelLogin(tel: string) {
    let row = await this.knex.select('id').from('user').where({ tel }).first()
    if (!row) {
      throw new Error('This tel is not registered')
    }
    let id = row.id

    let code = this.genCode()
    let message = `${code} is your verification code for MC App. If you didn't request SMS verification, do not pass this number to others.`
    await this.sms.sendSMS(tel, message)

    let timer = setTimeout(() => {
      this.code_verifyTel_map.delete(code)
    }, 5 * 1000 * 60)
    this.code_verifyTel_map.set(code, { id, tel, timer })

    return {}
  }

  async requestChangeTel(id: number, tel: string) {
    let row = await this.knex
      .count('* as count')
      .from('user')
      .where({ tel })
      .first()
    let count = +row!.count
    console.log({ row })
    if (count > 0) {
      throw new Error('The requested tel is already used')
    }

    let code = this.genCode()
    let message = `${code} is your verification code for MC App. If you didn't request SMS verification, do not pass this number to others.`
    await this.sms.sendSMS(tel, message)

    let timer = setTimeout(() => {
      this.code_verifyTel_map.delete(code)
    }, 5 * 1000 * 60)
    this.code_verifyTel_map.set(code, { id, tel, timer })

    return {}
  }

  async verifyTelLogin(code: string) {
    let pair = this.code_verifyTel_map.get(code)
    if (!pair) {
      throw new Error('wrong or expired code')
    }

    let payload: JWTPayload = {
      id: pair.id,
    }
    let token = jwtSimple.encode(payload, this.JWT_SECRET)

    clearTimeout(pair.timer)
    this.code_verifyTel_map.delete(code)

    return { token }
  }

  async verifyChangeTel(code: string) {
    let pair = this.code_verifyTel_map.get(code)
    if (!pair) {
      throw new Error('wrong or expired code')
    }
    await this.knex('user').update({ tel: pair.tel }).where({ id: pair.id })
    clearTimeout(pair.timer)
    this.code_verifyTel_map.delete(code)

    return {}
  }

  async getProfile(id: number) {
    let user = await this.knex.select('*').from('user').where({ id }).first()
    if (!user) {
      throw new Error('User not found')
    }
    return { user }
  }

  async createGuest(profile: NewProfile) {
    let [id] = await this.knex.insert(profile).into('user').returning('id')
    let payload: JWTPayload = {
      id: id as number,
    }
    let token = this.createToken(payload)
    return { token }
  }

  private createToken(payload: JWTPayload): string {
    let token = jwtSimple.encode(payload, this.JWT_SECRET)
    return token
  }
}
