import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('grade', t => {
    t.increments('id')
    t.string('name').notNullable()
  })
  await knex
    .insert(
      ['一年級', '二年級', '三年級', '四年級', '五年級', '六年級'].map(
        name => ({ name }),
      ),
    )
    .into('grade')
  await knex.schema.alterTable('user', t => {
    t.integer('grade_id').nullable().references('grade.id')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('user', t => {
    t.dropColumn('grade_id')
  })
  await knex.schema.dropTable('grade')
}
