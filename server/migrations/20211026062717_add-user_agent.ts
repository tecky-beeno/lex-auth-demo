import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('user', t => {
    t.string('user_agent').notNullable().defaultTo('unknown')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('user', t => {
    t.dropColumn('user_agent')
  })
}
