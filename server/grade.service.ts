import { Knex } from 'knex'

export class GradeService {
  constructor(public knex: Knex) {}
  async getList() {
    let grade_list = await this.knex.select('id', 'name').from('grade')
    return { grade_list }
  }
}
