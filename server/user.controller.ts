import { UserService } from './user.service'
import express, { Request, Response } from 'express'
import { Controller } from './controller'
import { GuardController } from './guard.controller'

export class UserController extends Controller {
  constructor(public userService: UserService, public guard: GuardController) {
    super()
    this.router.post('/user/guest', this.createGuest)
    this.router.get('/user/profile', this.guard.parseToken, this.getProfile)

    this.router.post('/user/name', this.guard.parseToken, this.changeName)

    this.router.post(
      '/user/tel/request',
      this.guard.parseToken,
      this.requestChangeTel,
    )
    this.router.post(
      '/user/tel/verify',
      this.guard.parseToken,
      this.verifyChangeTel,
    )

    this.router.post('/user/login/tel', this.requestTelLogin)
    this.router.post('/user/login/verify', this.verifyTelLogin)
  }

  verifyTelLogin = (req: Request, res: Response) => {
    let { code } = req.body
    if (!code) {
      res.status(400).json({ error: 'missing code in req.body' })
      return
    }
    this.callAPI(req, res, () => this.userService.verifyTelLogin(code))
  }

  requestTelLogin = (req: Request, res: Response) => {
    let { tel } = req.body
    if (!tel) {
      res.status(400).json({ error: 'missing tel in req.body' })
      return
    }
    this.callAPI(req, res, () => this.userService.requestTelLogin(tel))
  }

  changeName = (req: Request, res: Response) => {
    let id = req.jwt.id
    let { name } = req.body
    if (!name) {
      res.status(400).json({ error: 'missing name in req.body' })
      return
    }
    this.callAPI(req, res, () => this.userService.changeName(id, name))
  }

  getProfile = (req: Request, res: Response) => {
    let id = req.jwt.id
    this.callAPI(req, res, () => this.userService.getProfile(id))
  }

  requestChangeTel = (req: Request, res: Response) => {
    let { tel } = req.body
    let user = req.jwt
    if (!tel) {
      res.status(400).json({ error: 'missing tel in req.body' })
      return
    }
    this.callAPI(req, res, () =>
      this.userService.requestChangeTel(user.id, tel),
    )
  }

  verifyChangeTel = (req: Request, res: Response) => {
    let { code } = req.body
    if (!code) {
      res.status(400).json({ error: 'missing code in req.body' })
      return
    }
    this.callAPI(req, res, () => this.userService.verifyChangeTel(code))
  }

  createGuest = (req: Request, res: Response) => {
    let { name, grade_id, user_agent } = req.body
    if (!name) {
      res.status(400).json({ error: 'missing name in req.body' })
      return
    }
    if (!grade_id) {
      res.status(400).json({ error: 'missing grade_id in req.body' })
      return
    }
    if (!user_agent) {
      res.status(400).json({ error: 'missing user_agent in req.body' })
      return
    }
    this.callAPI(req, res, () =>
      this.userService.createGuest({ name, grade_id, user_agent }),
    )
  }
}
