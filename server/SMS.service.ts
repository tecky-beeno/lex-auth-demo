import { config } from 'dotenv'
import fetch from 'node-fetch'

export class SMSService {
  constructor() {
    config()
  }
  async sendSMS(tel: string, message: string) {
    // TODO
    console.log('send SMS to', tel, 'message:', message)

    let account = process.env.SMS_ACCOUNT
    if (!account) {
      throw new Error('missing SMS_ACCOUNT in env')
    }

    let token = process.env.SMS_API_KEY
    if (!token) {
      throw new Error('missing SMS_API_KEY in env')
    }

    let res = await fetch(
      `https://sms.8x8.com/api/v1/subaccounts/${account}/messages`,
      {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          source: 'MC System', // max 16 chars
          destination: tel,
          text: message,
          encoding: 'AUTO',
        }),
      },
    )

    if (!res.ok) {
      console.log('Failed to send SMS:', await res.text())
      throw new Error('Failed to send SMS: ' + res.statusText)
    }
  }
}
