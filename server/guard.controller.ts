import { Knex } from 'knex'
import jwtSimple from 'jwt-simple'
import express, { Request, Response, NextFunction } from 'express'
import { Bearer } from 'permit'
import { JWTPayload } from './types'

export class GuardController {
  constructor(private JWT_SECRET: string) {}

  private permit = new Bearer({ query: 'access_token' })

  parseToken = (req: Request, res: Response, next: NextFunction) => {
    let token: string
    try {
      token = this.permit.check(req)
    } catch (error) {
      res.status(400).json({ error: 'invalid Bearer Token' })
      return
    }
    let payload: JWTPayload
    try {
      payload = jwtSimple.decode(token, this.JWT_SECRET)
    } catch (error) {
      res.status(400).json({ error: 'invalid JWT' })
      return
    }
    req.jwt = payload
    next()
  }
}
