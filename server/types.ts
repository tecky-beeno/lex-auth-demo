export type JWTPayload = {
  id: number
}

declare global {
  namespace Express {
    interface Request {
      jwt: JWTPayload
    }
  }
}

export type NewProfile = {
  name: string
  grade_id: number
  user_agent: string
}
