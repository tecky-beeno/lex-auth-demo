import express, { Request, Response } from 'express'

export class Controller {
  router = express.Router()

  callAPI = async (req: Request, res: Response, fn: () => any) => {
    try {
      let json = await fn()
      res.json(json)
    } catch (error) {
      res.status(500).json({ error: (error as Error).toString() })
    }
  }
}
