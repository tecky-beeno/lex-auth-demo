import { config } from 'dotenv'

config()

function loadEnv(name: string, defaultValue?: string) {
  let value = process.env[name] || defaultValue
  if (!value) {
    throw new Error(`missing ${name} in env`)
  }
  return value
}

export let JWT_SECRET = loadEnv('JWT_SECRET')

export let PORT = +loadEnv('PORT', '8100')
export let NODE_ENV = loadEnv('NODE_ENV', 'development')
