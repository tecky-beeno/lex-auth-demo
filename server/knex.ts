import Knex from 'knex'
import { NODE_ENV } from './env'

let profiles = require('./knexfile')
let profile = profiles[NODE_ENV]

export let knex = Knex(profile)
