import { GradeService } from './grade.service'
import express, { Request, Response } from 'express'
import { Controller } from './controller'

export class GradeController extends Controller {
  constructor(public gradeService: GradeService) {
    super()
    this.router.get('/grade', this.getList)
  }

  getList = (req: Request, res: Response) => {
    this.callAPI(req, res, () => this.gradeService.getList())
  }
}
