import { IonAlert, IonText } from '@ionic/react'
import './ErrorMessage.css'

const ErrorMessage = (props: { error: any }) => {
  let error = props.error
  if (!error) {
    return <></>
  }
  if (error instanceof Error) {
    error = error.toString()
  } else if (typeof error !== 'string') {
    error = JSON.stringify(error, null, 2)
  }
  error = error.replace('Error: Error:', 'Error:')
  return <>{props.error ? <pre className="ErrorMessage">{error}</pre> : null}</>
}

export default ErrorMessage
