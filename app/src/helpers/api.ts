let server_origin = 'http://127.0.0.1:8100'

export async function get(url: string) {
  let res = await fetch(server_origin + url, {
    headers: {
      Authorization: 'Bearer ' + JSON.parse(localStorage.getItem('token')!),
    },
  })
  let json = await res.json()
  if (json.error) {
    console.error('GET failed:', { url, error: json.error })
    throw new Error(json.error)
  }
  return json
}

export async function post(url: string, body: any) {
  let res = await fetch(server_origin + url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + JSON.parse(localStorage.getItem('token')!),
    },
    body: JSON.stringify(body),
  })
  let json = await res.json()
  if (json.error) {
    console.error('GET failed:', { url, error: json.error })
    throw new Error(json.error)
  }
  return json
}
