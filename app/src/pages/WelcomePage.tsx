import {
  IonContent,
  IonHeader,
  IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { logoYoutube } from 'ionicons/icons'

/* <Redirect to="/tab1" /> */

const WelcomePage: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen className="ion-padding">
        <h1>Welcome to MC App</h1>
        <img src={logoYoutube} />
        <IonList>
          <IonItem routerLink="/login">登入帳戶</IonItem>
          <IonItem routerLink="/setup">直接使用</IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default WelcomePage
