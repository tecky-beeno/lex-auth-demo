import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useLocation } from 'react-router'
import ExploreContainer from '../components/ExploreContainer'
import './Tab3.css'

const NotMatchPage: React.FC = () => {
  let location = useLocation()
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Not Found</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <pre>
          <code>{JSON.stringify(location, null, 2)}</code>
        </pre>
      </IonContent>
    </IonPage>
  )
}

export default NotMatchPage
