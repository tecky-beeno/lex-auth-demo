import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonLoading,
  IonModal,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonText,
  IonTitle,
  IonToast,
  IonToolbar,
  useIonRouter,
} from '@ionic/react'
import { useEffect, useState } from 'react'
import ErrorMessage from '../components/ErrorMessage'
import { get, post } from '../helpers/api'
import 'react-use-storage-state'
import useStorageState from 'react-use-storage-state'

type Grade = {
  id: number
  name: string
}

const NewProfilePage: React.FC = () => {
  const [grade_list, set_grade_list] = useState<Grade[]>([])
  const [error, set_error] = useState()
  const [profile, setProfile] = useState({
    name: '',
    grade_id: null,
    user_agent: navigator.userAgent,
  })
  const [token, setToken] = useStorageState('token', '')
  useEffect(() => {
    get('/grade')
      .then(json => set_grade_list(json.grade_list))
      .catch(set_error)
  }, [])
  let router = useIonRouter()
  function submitProfile() {
    post('/user/guest', profile)
      .then(json => {
        setToken(json.token)
        router.push('/tabs/home', 'root')
      })
      .catch(set_error)
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Setup Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <ErrorMessage error={error} />
        <IonList>
          <IonItem>
            <IonLabel position="floating">Nickname</IonLabel>
            <IonInput
              value={profile.name}
              onIonChange={e =>
                setProfile({ ...profile, name: e.detail.value || '' })
              }
            />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">年級</IonLabel>
            <IonSelect
              value={profile.grade_id}
              onIonChange={e =>
                setProfile({ ...profile, grade_id: e.detail.value })
              }
            >
              {grade_list.map(grade => (
                <IonSelectOption key={grade.id} value={grade.id}>
                  {grade.name}
                </IonSelectOption>
              ))}
            </IonSelect>
          </IonItem>
        </IonList>
        <IonButton expand="block" onClick={submitProfile}>
          開始
        </IonButton>
        <ErrorMessage error={profile} />
      </IonContent>
    </IonPage>
  )
}

export default NewProfilePage
