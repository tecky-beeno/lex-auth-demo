import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { refreshSharp } from 'ionicons/icons'
import { useEffect, useState } from 'react'
import useStorageState from 'react-use-storage-state'
import ErrorMessage from '../components/ErrorMessage'
import { get, post } from '../helpers/api'

type Profile = {
  tel: string
  name: string
}

const SettingsTab: React.FC = () => {
  const [token, setToken] = useStorageState('token', '')
  const [profile, setProfile] = useState<Profile>({} as any)
  const [error, setError] = useState(null)
  const [tel, setTel] = useState('')
  const [name, setName] = useState('')
  const [verifyCode, setVerifyCode] = useState('')
  const [sent_code, set_sent_code] = useState(false)
  function loadProfile() {
    get('/user/profile')
      .then(json => {
        setProfile(json.user)
        setTel(json.user.tel)
        setName(json.user.name)
        setError(null)
      })
      .catch(setError)
  }
  useEffect(loadProfile, [token])
  function changeName() {
    post('/user/name', { name })
      .then(json => {
        setProfile({ ...profile, name })
        setError(null)
      })
      .catch(setError)
  }
  function changeTel() {
    post('/user/tel/request', { tel })
      .then(json => {
        set_sent_code(true)
        setError(null)
      })
      .catch(setError)
  }
  function verifyChangeTel() {
    post('/user/tel/verify', { code: verifyCode })
      .then(json => {
        set_sent_code(true)
        setError(null)
      })
      .catch(setError)
  }
  function logout() {
    setToken('')
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Settings</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={loadProfile}>
              <IonIcon icon={refreshSharp}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Settings</IonTitle>
          </IonToolbar>
        </IonHeader>

        <ErrorMessage error={error} />

        <IonList>
          <IonItem hidden>token: {token}</IonItem>
          <IonItem hidden>
            profile: <ErrorMessage error={profile} />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Nickname</IonLabel>
            <IonInput
              type="text"
              value={name}
              onIonChange={e => setName(e.detail.value || '')}
            />
            <IonButton
              slot="end"
              onClick={changeName}
              disabled={profile.name === name}
            >
              Save
            </IonButton>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Tel</IonLabel>
            <IonInput
              type="tel"
              value={tel}
              onIonChange={e => setTel(e.detail.value || '')}
            />
            <IonButton
              slot="end"
              onClick={changeTel}
              disabled={profile.tel === tel || tel.length !== 8}
            >
              Save
            </IonButton>
          </IonItem>
          <IonItem hidden={!sent_code}>
            <IonLabel position="floating">SMS Code</IonLabel>
            <IonInput
              type="number"
              value={verifyCode}
              onIonChange={e => setVerifyCode(e.detail.value || '')}
            />
            <IonButton
              slot="end"
              onClick={verifyChangeTel}
              disabled={verifyCode.length != 6}
            >
              Verify
            </IonButton>
          </IonItem>
          <div className="ion-text-center" style={{ marginTop: '5em' }}>
            <IonButton color="dark" routerLink="/" onClick={logout}>
              Logout
            </IonButton>
          </div>
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default SettingsTab
