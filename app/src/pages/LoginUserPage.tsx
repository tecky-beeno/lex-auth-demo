import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from '@ionic/react'
import { useState } from 'react'
import useStorageState from 'react-use-storage-state'
import ErrorMessage from '../components/ErrorMessage'
import ExploreContainer from '../components/ExploreContainer'
import { post } from '../helpers/api'
import './Tab1.css'
import { useToken } from '../hooks/use-token'

const LoginPage: React.FC = () => {
  const [tel, setTel] = useState('')
  const [error, setError] = useState(null)
  const [sent_code, set_sent_code] = useState(false)
  const [verifyCode, setVerifyCode] = useState('')
  const [token, setToken] = useToken()
  const router = useIonRouter()
  function requestTelLogin() {
    post('/user/login/tel', { tel })
      .then(() => {
        set_sent_code(true)
        setError(null)
      })
      .catch(setError)
  }
  function verifyTelLogin() {
    post('/user/login/verify', { code: verifyCode })
      .then(json => {
        setError(null)
        setToken(json.token)
        router.push('/tabs/home', 'root')
      })
      .catch(setError)
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <ErrorMessage error={error} />
        <IonList>
          <IonItem>
            <IonLabel position="stacked">Phone Number</IonLabel>
            <IonInput
              type="tel"
              value={tel}
              onIonChange={e => setTel(e.detail.value || '')}
            />
            <IonButton
              slot="end"
              onClick={requestTelLogin}
              disabled={tel.length != 8}
            >
              Verify
            </IonButton>
          </IonItem>
          <IonItem>
            <IonLabel position="stacked">Verify Code</IonLabel>
            <IonInput
              type="number"
              value={verifyCode}
              onIonChange={e => setVerifyCode(e.detail.value || '')}
              disabled={!sent_code}
            />
          </IonItem>
        </IonList>
        <IonButton
          expand="block"
          disabled={verifyCode.length != 6}
          onClick={verifyTelLogin}
        >
          Login
        </IonButton>
      </IonContent>
    </IonPage>
  )
}

export default LoginPage
