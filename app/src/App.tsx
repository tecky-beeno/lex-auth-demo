import { Redirect, Route } from 'react-router-dom'
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
import {
  barChartSharp,
  cogOutline,
  cogSharp,
  ellipse,
  homeSharp,
  linkSharp,
  settingsSharp,
  square,
  triangle,
} from 'ionicons/icons'
import Tab1 from './pages/Tab1'
import Tab2 from './pages/Tab2'
import SettingsTab from './pages/SettingsTab'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/typography.css'

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/float-elements.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/display.css'

/* Theme variables */
import './theme/variables.css'
import WelcomePage from './pages/WelcomePage'
import NotMatchPage from './pages/NotMatchPage'
import LoginUserPage from './pages/LoginUserPage';
import NewProfilePage from './pages/NewProfilePage';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/" component={WelcomePage} />
        <Route exact path="/login" component={LoginUserPage} />
        <Route exact path="/setup" component={NewProfilePage} />
        <Route path="/tabs" component={Tabs} />
        <Route component={NotMatchPage} />
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
)

const Tabs = () => {
  return (
    <IonTabs>
      <IonRouterOutlet>
        <Route exact path="/tabs/home">
          <Tab1 />
        </Route>
        <Route exact path="/tabs/record">
          <Tab2 />
        </Route>
        <Route path="/tabs/settings">
          <SettingsTab />
        </Route>
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="tab1" href="/tabs/home">
          <IonIcon icon={homeSharp} />
          <IonLabel>Home</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab2" href="/tabs/record">
          <IonIcon icon={barChartSharp} />
          <IonLabel>Record</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab3" href="/tabs/settings">
          <IonIcon md={settingsSharp} ios={cogSharp} />
          <IonLabel>Settings</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  )
}

export default App
